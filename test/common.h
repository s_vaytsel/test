#ifndef COMMON_H
#define COMMON_H

// TODO: This header was as a crutch for the GNU Compiler
// Warning: the header should be included after all other includes

#ifdef __GNUC__
#define __in
#define __out
#define __inout
#endif // __GNUC__

#endif // COMMON_H
