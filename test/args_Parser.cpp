#include "pch.h"

#include "args_Parser.h"

#ifdef _MSC_VER
#pragma warning(push) //For /Wall
#pragma warning(disable : 4100)
#pragma warning(disable : 4456)
#pragma warning(disable : 4458)
#endif // _MSC_VER

#include <args/args.hxx>

#ifdef _MSC_VER
#pragma warning(pop) //For /Wall
#endif // _MSC_VER

using std::cout;
using std::cerr;
using std::endl;
using std::string;
using std::runtime_error;
using std::make_unique;

args::Parser::Parser (int argc, char ** argv) {
  const string helpMessage = "\
USAGE:\n\n\
1. test -f Test.tst -m words -v mother\n\
Prints the number of words \"mother\" in the file \"Test.tst\"\n\n\
2. test -f Test.tst -m checksum\n\
Prints a 32-bit checksum calculated by a algorithm\n\
checksum = word1 + word2 + ... + wordN\n\
(word1..wordN - 32-bit words representing the contents of a file \"Test.tst\")\n\n\
3. test -h\n\
Prints this test";

  ArgumentParser parser ("This is a test program.", helpMessage);
  ValueFlag<string> file (parser, "file", "The name of file", {'f'});
  ValueFlag<string> commandType (parser, "message", "Command type. One of ['words', 'checksum']", {'m'});
  ValueFlag<string> optionalParameter (parser, "value", "'words' command parameter", {'v'});

  try {
    parser.ParseCLI (argc, argv);
  } catch (const runtime_error e) {
    cerr << e.what () << endl;
    cerr << parser;
    throw e;
  }

  const auto commandTypeStr = commandType.Get ();

  if ("words" == commandTypeStr) {
    const auto filePath = file.Get ();
    const auto word = optionalParameter.Get ();
    this->wordSearcherParameters = make_unique<search::Parameters> (search::Parameters {filePath, word});
    this->command = Command::Search;
  } else if ("checksum" == commandTypeStr) {
    const auto filePath = file.Get ();
    this->checksumParameters = make_unique<checksum::Parameters> (checksum::Parameters {filePath});
    this->command = Command::CheckSum;
  } else {
    cerr << parser;
    throw runtime_error (("Wrong -m parameter value: " + commandTypeStr).c_str ());
  }

} // namespace args
