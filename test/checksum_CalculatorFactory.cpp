#include "pch.h"

#include "checksum_Parameters.h"
#include "checksum_CalculatorFactory.h"
#include "checksum_SequentialCalculator.h"
#include "checksum_ParallelCalculator.h"

using std::unique_ptr;
using std::make_unique;

namespace checksum {

  CalculatorFactory::CalculatorFactory (std::unique_ptr<Parameters>& p): parameters (std::move (p)) {}

  unique_ptr<checksum::Calculator> CalculatorFactory::get () {
    const int numProcs = omp_get_num_procs ();

    // TODO: if processors supports SSE instructions, use them

    if (numProcs == 1) {
      return unique_ptr<Calculator> (dynamic_cast<Calculator*>(new SequentialCalculator (parameters)));
    } else {
      return unique_ptr<Calculator> (dynamic_cast<Calculator*>(new ParallelCalculator (parameters)));
    }
  }
} // namespace checksum
