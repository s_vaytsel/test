#include "pch.h"

#include "args_Parser.h"
#include "search_WordSearcher.h"
#include "search_WordSearcherFactory.h"
#include "checksum_Calculator.h"
#include "checksum_CalculatorFactory.h"

using std::cout;
using std::cerr;
using std::endl;
using std::move;
using std::exception;
using std::unique_ptr;

int main (int argc, char** argv) {
  try {
    args::Parser argsParser (argc, argv);

    switch (argsParser.command) {
      case args::Parser::Command::Search:
        {
          search::WordSearcherFactory factory (argsParser.wordSearcherParameters);
          const unique_ptr<search::WordSearcher> searcher = std::move (factory.get ());
          auto result = searcher->run ();
          cout << result << endl;
          break;
        }
      case args::Parser::Command::CheckSum:
        {
          checksum::CalculatorFactory factory (argsParser.checksumParameters);
          const unique_ptr<checksum::Calculator> calculator = std::move (factory.get ());
          auto result = calculator->run ();
          cout << result << endl;
          break;
        }
    }
  } catch (const exception& e) {
    cerr << e.what () << endl;
    return 1;
  }

  return 0;
}
