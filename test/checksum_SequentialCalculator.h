#ifndef CHECKSUM_SEQ_CALCULATOR_H
#define CHECKSUM_SEQ_CALCULATOR_H

#include <array>
#include <memory>

#include "checksum_Parameters.h"
#include "checksum_Calculator.h"

#include "common.h"

namespace checksum {

  class SequentialCalculator: public Calculator {
    unsigned int process (__in const std::array<int32_t, Parameters::bufferSize> & buffer,
                          __in const size_t bufferFullness,
                          __in const bool eof);
  public:
    explicit SequentialCalculator (std::unique_ptr<Parameters>& p): Calculator (p) {};
    virtual unsigned int run () override final;
  };

} // namespace checksum

#endif // CHECKSUM_SEQ_CALCULATOR_H
