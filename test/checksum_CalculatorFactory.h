#ifndef CHECKSUM_CALCULATOR_FACTORY_H
#define CHECKSUM_CALCULATOR_FACTORY_H

#include <memory>

#include "checksum_Parameters.h"
#include "checksum_Calculator.h"

namespace checksum {
  class CalculatorFactory {
    std::unique_ptr<Parameters> parameters;
  public:
    explicit CalculatorFactory (std::unique_ptr<Parameters>&);
    std::unique_ptr<Calculator> get ();
  };
} // namespace checksum

#endif // CHECKSUM_CALCULATOR_FACTORY_H
