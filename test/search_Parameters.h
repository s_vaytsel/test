#ifndef SEARCH_PARAMETERS_H
#define SEARCH_PARAMETERS_H

#include <string>

namespace search {

  struct Parameters {
    std::string filePath;
    std::string word;

    static const size_t bufferSize = 131072U;
    static const std::string separators;
  };

} // namespace search

#endif // SEARCH_PARAMETERS_H
