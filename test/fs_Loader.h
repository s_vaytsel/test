#ifndef FS_LOADER_H
#define FS_LOADER_H

#include <fstream>
#include <string>
#include <array>

#include "common.h"

namespace fs {

  class Loader {
    std::ifstream ifs;
  public:
    explicit Loader (const std::string & filePath);
    ~Loader ();

    template<typename arrayType, size_t bufferSize>
    void sequentialLoad (__out std::array<arrayType, bufferSize> & buffer,
                         __out size_t & bufferFullness,
                         __out bool & eof,
                         __out unsigned int & error) noexcept;

    template<typename arrayType, size_t bufferSize>
    void parallelLoad (__out std::array<arrayType, bufferSize> & buffer,
                       __out size_t & bufferFullness,
                       __out bool & eof,
                       __out unsigned int & error) noexcept;

    template<typename arrayType, size_t bufferSize>
    void sequentialLoadWithShadow (__in const size_t shadowWidth,
                                   __out std::array<arrayType, bufferSize> & buffer,
                                   __out size_t & bufferFullness,
                                   __out bool & eof,
                                   __out unsigned int & error) noexcept;

    template<typename arrayType, size_t bufferSize>
    void parallelLoadWithShadow (__in const size_t shadowWidth,
                                 __in const std::array<arrayType, bufferSize>& oldBuffer,
                                 __out std::array<arrayType, bufferSize> & buffer,
                                 __out size_t & bufferFullness,
                                 __out bool & eof,
                                 __out unsigned int & error) noexcept;
  };

} // namespace fs

#endif // FS_LOADER_H
