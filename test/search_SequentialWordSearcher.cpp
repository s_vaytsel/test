#include "pch.h"

#include "search_SequentialWordSearcher.h"
#include "fs_Loader.inl"

using std::array;

namespace search {

  unsigned int SequentialWordSearcher::process (__in const array<char, Parameters::bufferSize> & buffer,
                                                __in const size_t bufferFullness,
                                                __in const bool eof) {
    const auto b = cbegin (buffer);
    const auto wordLength = parameters->word.length ();
    static const auto unprocessibleLength = wordLength - 1;
    const auto e = eof
      ? b + (bufferFullness - unprocessibleLength)
      : cend (buffer) - unprocessibleLength;

    unsigned int counter = 0;

    auto i = b + 1, j = b;
    if (wordStartedBy (b, e)) {
      counter++;
      i += wordLength;
      j += wordLength;
    }

    for (; i < e; i++, j++) {
      if (isSeparator (*j) && wordStartedBy (i, e)) {
        counter++;
        i += wordLength;
        j += wordLength;
      }
    }

    return counter;
  }

  unsigned int SequentialWordSearcher::run () {
    alignas(16) array<char, Parameters::bufferSize> buffer {};
    bool eof = false;
    size_t bufferFullness = 0;
    const auto shadowWidth = parameters->word.length () - 1;

    unsigned int counter = 0;
    unsigned int error = 0;

    while (!eof && error == 0) {
      loader.sequentialLoadWithShadow (shadowWidth, buffer, bufferFullness, eof, error);
      counter += process (buffer, bufferFullness, eof);
    }

    return counter;
  }

} // namespace search
