#include "pch.h"

#include "search_ParallelWordSearcher.h"
#include "fs_Loader.inl"

using std::array;
using std::runtime_error;

namespace search {

  void ParallelWordSearcher::process (__in const std::array<char, Parameters::bufferSize> & buffer,
                                      __in const size_t bufferFullness,
                                      __in const bool eof,
                                      __in const unsigned int threadId,
                                      __in const unsigned int numThreads,
                                      __out unsigned int & error,
                                      __inout unsigned int & counter) noexcept {
    (void) error;

    const auto b = cbegin (buffer);
    const size_t wordLength = parameters->word.length ();
    static const size_t unprocessibleLength = wordLength - 1;
    const auto e = eof
      ? b + (bufferFullness - unprocessibleLength)
      : cend (buffer) - unprocessibleLength;
    const ptrdiff_t pieceSize = (e - b) / (numThreads - 1);
    const auto bi = b + pieceSize * (threadId - 1);
    const auto ei = (threadId == numThreads - 1) ? e : bi + pieceSize;

    auto i = bi + 1, j = bi;
    if ((b == bi || isSeparator (*(bi - 1))) && wordStartedBy (bi, ei)) {
      counter++;
      i += wordLength;
      j += wordLength;
    }

    for (; i < ei; i++, j++) {
      if (isSeparator (*j) && wordStartedBy (i, ei)) {
        counter++;
        i += wordLength;
        j += wordLength;
      }
    }
  }

  unsigned int ParallelWordSearcher::run () {
    alignas(16) array<char, Parameters::bufferSize> buffer1 {};
    alignas(16) array<char, Parameters::bufferSize> buffer2 {};
    bool eof1 = false, eof2 = false;
    size_t buffer1Fullness = 0, buffer2Fullness = 0;

    const size_t shadowWidth = parameters->word.length () - 1;
    const unsigned int numProcs = omp_get_num_procs ();

    unsigned int counter = 0;
    unsigned int loaderError = 0, processError = 0;

    omp_set_num_threads (numProcs);

#pragma omp parallel reduction (+:counter)
    {
      unsigned int threadId = omp_get_thread_num ();
      unsigned int numThreads = omp_get_num_threads ();

      if (threadId == 0) {
        loader.parallelLoadWithShadow (shadowWidth, buffer2, buffer1, buffer1Fullness, eof1, loaderError);
#pragma omp barrier
        while (!eof1 && !eof2 && loaderError == 0 && processError == 0) {
          loader.parallelLoadWithShadow (shadowWidth, buffer1, buffer2, buffer2Fullness, eof2, loaderError);
#pragma omp barrier
          if (!eof2 && loaderError == 0 && processError == 0) {
            loader.parallelLoadWithShadow (shadowWidth, buffer2, buffer1, buffer1Fullness, eof1, loaderError);
          }
#pragma omp barrier
        }
      } else {
        unsigned int threadCounter = 0;
        counter = 0;
#pragma omp barrier
        while (!eof1 && !eof2 && loaderError == 0 && processError == 0) {
          process (buffer1, buffer1Fullness, eof1, threadId, numThreads, processError, threadCounter);
#pragma omp barrier
          if (!eof2 && loaderError == 0 && processError == 0) {
            process (buffer2, buffer2Fullness, eof2, threadId, numThreads, processError, threadCounter);
          }
#pragma omp barrier
        }
        if (eof1 && loaderError == 0 && processError == 0) {
          process (buffer1, buffer1Fullness, eof1, threadId, numThreads, processError, threadCounter);
        }
        if (eof2 && loaderError == 0 && processError == 0) {
          process (buffer2, buffer2Fullness, eof2, threadId, numThreads, processError, threadCounter);
        }

        counter += threadCounter;
      }
    }

    if (processError != 0) {
      throw runtime_error("Some process error in OpenMP section happens");
    }
    if (loaderError != 0) {
      throw runtime_error ("Some loader error in OpenMP section happens");
    }

    return counter;
  }

} // namespace search
