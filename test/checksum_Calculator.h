#ifndef CHECKSUM_CALCULATOR_H
#define CHECKSUM_CALCULATOR_H

#include <memory>

#include "checksum_Parameters.h"
#include "fs_Loader.h"

namespace checksum {

  class Calculator {
  protected:
    std::unique_ptr<Parameters> parameters;

    fs::Loader loader;
  public:
    explicit Calculator (std::unique_ptr<Parameters>&);
    virtual unsigned int run () = 0;
  };

} // namespace checksum

#endif // CHECKSUM_CALCULATOR_H
