#ifndef ARGS_PARSER_H
#define ARGS_PARSER_H

#include <memory>

#include "search_Parameters.h"
#include "checksum_Parameters.h"

namespace args {

  class Parser {
  public:
    enum class Command {
      Search,
      CheckSum
    };
    std::unique_ptr<search::Parameters> wordSearcherParameters;
    std::unique_ptr<checksum::Parameters> checksumParameters;
    Command command;

    explicit Parser (int argc, char** argv);
  };

} // namespace args

#endif // ARGS_PARSER_H
