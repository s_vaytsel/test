#ifndef CHECKSUM_PAR_CALCULATOR_H
#define CHECKSUM_PAR_CALCULATOR_H

#include <array>
#include <memory>

#include "checksum_Parameters.h"
#include "checksum_Calculator.h"

#include "common.h"

namespace checksum {

  class ParallelCalculator: public Calculator {
    void process (__in const std::array<int32_t, Parameters::bufferSize> & buffer,
                  __in const size_t bufferFullness,
                  __in const bool eof,
                  __in const unsigned int threadId,
                  __in const unsigned int numThreads,
                  __out unsigned int & error,
                  __inout unsigned int & result) noexcept;
  public:
    explicit ParallelCalculator (std::unique_ptr<Parameters>& p): Calculator (p) {};
    virtual unsigned int run () override final;
  };

} // namespace checksum

#endif // CHECKSUM_PAR_CALCULATOR_H
