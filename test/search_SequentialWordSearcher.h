#ifndef SEARCH_SEQ_WORD_SEARCHER_H
#define SEARCH_SEQ_WORD_SEARCHER_H

#include <array>
#include <memory>

#include "search_Parameters.h"
#include "search_WordSearcher.h"

#include "common.h"

namespace search {

  class SequentialWordSearcher: public WordSearcher {
    unsigned int process (__in const std::array<char, Parameters::bufferSize> & buffer,
                          __in const size_t bufferFullness,
                          __in const bool eof);
  public:
    explicit SequentialWordSearcher (std::unique_ptr<Parameters>& p): WordSearcher (p) {};
    virtual unsigned int run () override final;
  };

} // namespace search

#endif // SEARCH_SEQ_WORD_SEARCHER_H
