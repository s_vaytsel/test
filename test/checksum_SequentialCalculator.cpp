#include "pch.h"

#include "checksum_SequentialCalculator.h"
#include "fs_Loader.inl"

using std::array;

namespace checksum {

  unsigned int SequentialCalculator::process (__in const std::array<int32_t, Parameters::bufferSize> & buffer,
                                              __in const size_t bufferFullness,
                                              __in const bool eof) {
    unsigned int result = 0;

    const auto b = cbegin (buffer);
    const auto e = eof ? b + bufferFullness + 1 : cend (buffer);

    for (auto i = b; i < e; i++) {
      result += *i;
    }

    return result;
  }

  unsigned int SequentialCalculator::run () {
    alignas(16) array<int32_t, Parameters::bufferSize> buffer {};
    bool eof = false;
    size_t bufferFullness = 0;

    unsigned int result = 0;
    unsigned int error = 0;

    while (!eof && error == 0) {
      loader.sequentialLoad (buffer, bufferFullness, eof, error);
      result += process (buffer, bufferFullness, eof);
    }

    return result;
  }

} // namespace checksum
