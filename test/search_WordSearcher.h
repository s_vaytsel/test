#ifndef SEARCH_WORD_SEARCHER_H
#define SEARCH_WORD_SEARCHER_H

#include <array>
#include <memory>

#include "search_Parameters.h"
#include "fs_Loader.h"

#include "common.h"

namespace search {

  class WordSearcher {
  protected:
    std::unique_ptr<Parameters> parameters;

    fs::Loader loader;

    bool wordStartedBy (const std::array<char, Parameters::bufferSize>::const_iterator &i,
                        const std::array<char, Parameters::bufferSize>::const_iterator &end) noexcept;

    bool isSeparator (__in const char ch) noexcept;
  public:
    explicit WordSearcher (std::unique_ptr<Parameters> & p);

    virtual unsigned int run () = 0;
  };

} // namespace search

#endif // SEARCH_WORD_SEARCHER_H
