#ifndef SEARCH_PAR_WORD_SEARCHER_H
#define SEARCH_PAR_WORD_SEARCHER_H

#include <array>
#include <memory>

#include "search_Parameters.h"
#include "search_WordSearcher.h"

#include "common.h"

namespace search {

  class ParallelWordSearcher: public WordSearcher {
    void process (__in const std::array<char, Parameters::bufferSize> & buffer,
                  __in const size_t bufferFullness,
                  __in const bool eof,
                  __in const unsigned int threadId,
                  __in const unsigned int numThreads,
                  __out unsigned int & error,
                  __inout unsigned int & counter) noexcept;
  public:
    explicit ParallelWordSearcher (std::unique_ptr<Parameters> & p): WordSearcher (p) {};
    virtual unsigned int run () override final;
  };

} // namespace search

#endif // SEARCH_PAR_WORD_SEARCHER_H
