#ifndef PCH_H
#define PCH_H

#ifdef _MSC_VER
#define WIN32_LEAN_AND_MEAN
#endif // _MSC_VER

#ifdef _MSC_VER
//For /Wall
#pragma warning(push)
#pragma warning(disable : 4820)
#pragma warning(disable : 4619)
#pragma warning(disable : 4548)
#pragma warning(disable : 4668)
#pragma warning(disable : 4365)
#pragma warning(disable : 4710)
#pragma warning(disable : 4371)
#pragma warning(disable : 4826)
#pragma warning(disable : 4061)
#pragma warning(disable : 4640)
#endif // _MSC_VER

#include <omp.h>

#include <algorithm>
#include <array>
#include <exception>
#include <fstream>
#include <iostream>
#include <memory>
#include <string>

#ifdef _MSC_VER
#pragma warning(pop) //For /Wall
#endif // _MSC_VER

#ifdef __GNUC__
#include <stddef.h>
#endif // __GNUC__

#endif // PCH_H
