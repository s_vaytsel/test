#ifndef CHECKSUM_PARAMETERS_H
#define CHECKSUM_PARAMETERS_H

#include <string>

namespace checksum {

  struct Parameters {
    static const size_t bufferSize = 32768U;

    std::string filePath;
  };

} // namespace checksum

#endif // CHECKSUM_PARAMETERS_H
