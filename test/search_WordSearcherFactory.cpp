#include "pch.h"

#include "search_WordSearcherFactory.h"
#include "search_SequentialWordSearcher.h"
#include "search_ParallelWordSearcher.h"

using std::unique_ptr;
using std::move;

namespace search {

  WordSearcherFactory::WordSearcherFactory (std::unique_ptr<Parameters>& p): parameters (std::move (p)) {}

  unique_ptr<search::WordSearcher> WordSearcherFactory::get () {
    const int numProcs = omp_get_num_procs ();

    if (numProcs == 1) {
      return unique_ptr<WordSearcher> (dynamic_cast<WordSearcher*>(new SequentialWordSearcher (parameters)));
    } else {
      return unique_ptr<WordSearcher> (dynamic_cast<WordSearcher*>(new ParallelWordSearcher (parameters)));
    }
  }

} // namespace search
