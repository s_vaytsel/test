#include "pch.h"

#include "search_WordSearcher.h"
#include "fs_Loader.inl"

using std::array;
using std::unique_ptr;
using std::move;
using std::runtime_error;

namespace search {

  bool WordSearcher::wordStartedBy (const array<char, Parameters::bufferSize>::const_iterator &i,
                                    const array<char, Parameters::bufferSize>::const_iterator &end) noexcept {
    const auto b = cbegin (parameters->word);
    const auto e = cend (parameters->word);
    auto i1 = i;
    for (auto i2 = b; i2 != e && i1 != end; i2++, i1++) {
      if (*i1 != *i2) return false;
    }
    return i1 == end || isSeparator (*i1);
  }

  bool WordSearcher::isSeparator (__in const char ch) noexcept {
    return Parameters::separators.find (ch) != Parameters::separators.npos;
  }

  WordSearcher::WordSearcher (unique_ptr<Parameters> & p): parameters (move (p)), loader (parameters->filePath) {
    for (auto ch : parameters->word) {
      if (isSeparator (ch)) {
        throw runtime_error (("Wrong word parameter, word have separators: " + parameters->word).c_str ());
      }
    }
  }

} // namespace search
