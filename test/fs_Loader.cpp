#include "pch.h"

#include "fs_Loader.h"

using std::string;
using std::runtime_error;

namespace fs {

  Loader::Loader (const string & filePath) {
    this->ifs.open (filePath);

    if (!this->ifs.good ()) {
      throw runtime_error (("Wrong file path: " + filePath).c_str ());
    }
  }

  Loader::~Loader () {
    ifs.close ();
  }

} // namespace fs
