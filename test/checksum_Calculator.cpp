#include "pch.h"

#include "checksum_Calculator.h"

using std::unique_ptr;

namespace checksum {

  Calculator::Calculator (unique_ptr<Parameters>& p): parameters (std::move (p)), loader (parameters->filePath) {}

} // namespace checksum
