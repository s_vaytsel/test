#include "pch.h"

#include "checksum_ParallelCalculator.h"
#include "fs_Loader.inl"

using std::array;
using std::runtime_error;

namespace checksum {

  void ParallelCalculator::process (__in const std::array<int32_t, Parameters::bufferSize> & buffer,
                                    __in const size_t bufferFullness,
                                    __in const bool eof,
                                    __in const unsigned int threadId,
                                    __in const unsigned int numThreads,
                                    __out unsigned int & error,
                                    __inout unsigned int & result) noexcept {
    (void) error;

    const auto b = cbegin (buffer);
    const auto e = eof ? b + bufferFullness + 1 : cend (buffer); // + 1 - it's for last i32 word, for zero-alignment
    const ptrdiff_t pieceSize = (e - b) / (numThreads - 1);
    const auto bi = b + pieceSize * (threadId - 1);
    const auto ei = (threadId == numThreads - 1) ? e : bi + pieceSize;

    for (auto i = bi; i < ei; i++) {
      result += *i;
    }
  }

  unsigned int ParallelCalculator::run () {
    alignas(16) array<int32_t, Parameters::bufferSize> buffer1 {};
    alignas(16) array<int32_t, Parameters::bufferSize> buffer2 {};
    bool eof1 = false, eof2 = false;
    size_t buffer1Fullness = 0, buffer2Fullness = 0;

    const unsigned int numProcs = omp_get_num_procs ();

    omp_set_num_threads (numProcs);

    unsigned int result = 0;

    unsigned int loaderError = 0, processError = 0;

#pragma omp parallel reduction (+:result)
    {
      unsigned int threadId = omp_get_thread_num ();
      unsigned int numThreads = omp_get_num_threads ();

      if (threadId == 0) {
        loader.parallelLoad (buffer1, buffer1Fullness, eof1, loaderError);
#pragma omp barrier
        while (!eof1 && !eof2 && loaderError == 0 && processError == 0) {
          loader.parallelLoad (buffer2, buffer2Fullness, eof2, loaderError);
#pragma omp barrier
          if (!eof2 && loaderError == 0 && processError == 0) {
            loader.parallelLoad (buffer1, buffer1Fullness, eof1, loaderError);
          }
#pragma omp barrier
        }
      } else {
        unsigned int threadResult = 0;
        result = 0;
#pragma omp barrier
        while (!eof1 && !eof2 && loaderError == 0 && processError == 0) {
          process (buffer1, buffer1Fullness, eof1, threadId, numThreads, processError, threadResult);
#pragma omp barrier
          if (!eof2 && loaderError == 0 && processError == 0) {
            process (buffer2, buffer2Fullness, eof2, threadId, numThreads, processError, threadResult);
          }
#pragma omp barrier
        }
        if (eof1 && loaderError == 0 && processError == 0) {
          process (buffer1, buffer1Fullness, eof1, threadId, numThreads, processError, threadResult);
        }
        if (eof2 && loaderError == 0 && processError == 0) {
          process (buffer2, buffer2Fullness, eof2, threadId, numThreads, processError, threadResult);
        }
        result += threadResult;
      }
    }

    if (processError != 0) {
      throw runtime_error ("Some process error in OpenMP section happens");
    }
    if (loaderError != 0) {
      throw runtime_error ("Some loader error in OpenMP section happens");
    }

    return result;
  }

} // namespace checksum
