#include "fs_Loader.h"

namespace fs {

  template<typename arrayType, size_t bufferSize>
  void Loader::sequentialLoad (__out std::array<arrayType, bufferSize> & buffer,
                               __out size_t & bufferFullness,
                               __out bool & eof,
                               __out unsigned int & error) noexcept {
    sequentialLoadWithShadow (0, buffer, bufferFullness, eof, error);
  }

  template<typename arrayType, size_t bufferSize>
  void Loader::parallelLoad (__out std::array<arrayType, bufferSize> & buffer,
                             __out size_t & bufferFullness,
                             __out bool & eof,
                             __out unsigned int & error) noexcept {
    parallelLoadWithShadow (0, buffer, buffer, bufferFullness, eof, error);
  }

  template<typename arrayType, size_t bufferSize>
  void Loader::sequentialLoadWithShadow (__in const size_t shadowWidth,
                                         __out std::array<arrayType, bufferSize> & buffer,
                                         __out size_t & bufferFullness,
                                         __out bool & eof,
                                         __out unsigned int & error) noexcept {
    for (size_t i = bufferSize - shadowWidth, j = 0; i < bufferSize; i++, j++) {
      buffer[j] = buffer[i];
    }

    if (ifs.good ()) {
      ifs.read (reinterpret_cast<char*>(&buffer[shadowWidth]), (bufferSize - shadowWidth) * sizeof (arrayType));

      if (!ifs) {
        bufferFullness = static_cast<size_t>(this->ifs.gcount ()) / sizeof (arrayType);
        eof = true;
      } else {
        bufferFullness = 0;
        eof = false;
      }

      error = 0;
    } else {
      error = 1;
    }
  }

  template<typename arrayType, size_t bufferSize>
  void Loader::parallelLoadWithShadow (__in const size_t shadowWidth,
                                       __in const std::array<arrayType, bufferSize>& oldBuffer,
                                       __out std::array<arrayType, bufferSize>& buffer,
                                       __out size_t & bufferFullness,
                                       __out bool & eof,
                                       __out unsigned int & error) noexcept {
    unsigned int _error = 0;

    for (size_t i = bufferSize - shadowWidth; i < bufferSize; i++) {
      buffer[i] = oldBuffer[i];
    }

    sequentialLoadWithShadow (shadowWidth, buffer, bufferFullness, eof, _error);

#pragma omp atomic
    error += _error;
  }

} // namespace fs
