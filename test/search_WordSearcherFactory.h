#ifndef SEARCH_WORD_SEARCHER_FACTORY_H
#define SEARCH_WORD_SEARCHER_FACTORY_H

#include "search_Parameters.h"
#include "search_WordSearcher.h"

namespace search {

  class WordSearcherFactory {
    std::unique_ptr<Parameters> parameters;
  public:
    explicit WordSearcherFactory (std::unique_ptr<Parameters>&);
    std::unique_ptr<WordSearcher> get ();
  };

} // namespace search

#endif // SEARCH_WORD_SEARCHER_FACTORY_H
